import json
import boto3
import base64
import simple_salesforce
import pandas as pd
from botocore.exceptions import ClientError


def handler(event, context):
    print("event:")
    print(event)
        
    USE_UAT = event["uat"]

    raw_secrets = json.loads(get_secret("sf_info"))
    sf_secrets = process_secrets(raw_secrets, USE_UAT)

    sf = simple_salesforce.Salesforce(username=sf_secrets["username"], 
                                      password=sf_secrets["password"],
                                      security_token=sf_secrets["security token"],
                                      domain=sf_secrets["domain"])

    objectType = event["objectType"]
    columns = ', '.join([col["queryField"] for col in event["columns"] if col["queryField"] != "checked"])
    bulk_portfolio_id = "a0B0R000002Hqy7" if event["bulkPortfolioId"] == "" else event["bulkPortfolioId"]

    if objectType == "jobs":
        sf_query = f"select {columns} from Job_or_Order__c where repair_project__r.property__r.bulk_portfolio__c = '{bulk_portfolio_id}' AND Status__c != 'Rejected' AND Status__c != 'Cancelled' AND Status__c != 'Completed'"
    elif objectType == "properties":
        sf_query = f"select {columns} from Property__c where Bulk_Portfolio__c = '{bulk_portfolio_id}' AND status__c != 'Lost' AND status__c != 'Sold'"
    
    print(f"query: {sf_query}")

    # Drop attributes from results
    results = [dict(val) for val in sf.query_all(sf_query)['records']]

    # Get nested values from relationship query fields
    for result in results:
        for k, v in result.items():
            if "__r" in k:
                result[k] = dict(v)
                del result[k]["attributes"]
                result[k] = list(result[k].values())[0]

    print(f"results: {results}")

    if len(results) == 0:
        objectList = []
        print(f"Query did not return any {objectType}")
        status_code = 401
    
    else:
        # Put results into DataFrame for easy manipulation
        results_df = pd.DataFrame(results).drop(columns=["attributes"])
        results_df.columns = [col["dataName"] for col in event["columns"] if col["dataName"] != "checked"]

        # Insert checked row for binding to checkbox
        results_df.insert(0, "checked", False)

        # Cast any int type columns
        for col in event["columns"]:
            if col["dataType"] == "int":
                results_df[col["dataName"]].loc[results_df[col["dataName"]] == "NA"] = 0
                results_df[col["dataName"]] = results_df[col["dataName"]].fillna(0).astype(int)
            if col["dataType"] == "currency":
                results_df[col["dataName"]].loc[results_df[col["dataName"]] == "NA"] = 0
                results_df[col["dataName"]] = results_df[col["dataName"]].fillna(0).astype(float)
                results_df[col["dataName"]] = results_df[col["dataName"]].apply(format_currency)

        # Extract vendor name from <a> tag
        if "Vendor_Contact__c" in results_df.columns:
            results_df = results_df.apply(extract_vendor, axis=1)

        print("results df:")
        print(results_df)

        # Convert df to list of dicts
        objectList = results_df.to_dict("records")
        status_code = 200
    
    body = json.dumps({"objectList": objectList})
    print(objectList)
    return {
        "statusCode": status_code,
        "body": body
    }
    

def extract_vendor(x: pd.Series):
    x["Vendor_Contact__c"] = x["Vendor_Contact__c"].split(">")[1].split("<")[0]
    return x


def format_currency(x: pd.Series):
    return "${:,.2f}".format(x)


def get_secret(secret_name: str):
    region_name = "us-east-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        else:
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            secret = base64.b64decode(get_secret_value_response['SecretBinary'])
        return secret


def process_secrets(secrets: dict, uat: bool):
    print(f"uat in process_secrets(): {uat}")
    print(f"uat type: {type(uat)}")
    username = secrets["sf_user"] + (".uat2" if uat == True else "")
    password = secrets["PWD"]
    security_token = secrets["UAT2_TOKEN" if uat == True else "PROD_TOKEN"]
    domain = "test" if uat == True else None

    print("sf params:")
    print(f"username: {username}")
    print(f"password: {password}")
    print(f"security_token: {security_token}")
    print(f"domain: {domain}")

    return {"username": username, "password": password, "security token": security_token, "domain": domain}
