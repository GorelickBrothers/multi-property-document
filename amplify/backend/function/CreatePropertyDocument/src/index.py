import sys
import traceback
import json
import boto3
import base64
import urllib
import simple_salesforce
from simple_salesforce.exceptions import SalesforceMalformedRequest
from botocore.exceptions import ClientError


s3 = boto3.client('s3')


def handler(event, context):
    print("event:")
    print(event)

    # event["object"] is the new property document
    item = event["object"]

    USE_UAT = event["uat"]
    file_path = event["filePath"]

    BUCKET_NAME = f"homeworx-{'dev' if USE_UAT == True else 'prod'}"
    file_url = f"https://{urllib.parse.quote_plus(BUCKET_NAME, safe='/')}.s3.amazonaws.com/{urllib.parse.quote_plus(file_path, safe='/')}".replace(" ", "+")

    raw_secrets = json.loads(get_secret("sf_info"))
    sf_secrets = process_secrets(raw_secrets, USE_UAT)

    sf = simple_salesforce.Salesforce(username=sf_secrets["username"],
                                      password=sf_secrets["password"],
                                      security_token=sf_secrets["security token"],
                                      domain=sf_secrets["domain"])

    item.update({"File_URL__c": file_url})

    print("New property document field values:")
    print(item)

    try:
        sf.Property_Document__c.create(item)
        status_code = 200
        body = json.dumps({"file_url": file_url})
    except SalesforceMalformedRequest:
        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = {"errorType": exception_type.__name__,
                   "errorMessage": str(exception_value),
                   "stackTrace": traceback_string}
        print(json.dumps(err_msg))
        status_code = 401
        body = json.dumps({"error": err_msg})

    return {
        "statusCode": status_code,
        "body": body
    }


def get_secret(secret_name: str):
    region_name = "us-east-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=region_name)

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        else:
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            secret = base64.b64decode(
                get_secret_value_response['SecretBinary'])
        return secret


def process_secrets(secrets: dict, uat: bool):
    username = secrets["sf_user"] + (".uat2" if uat == True else "")
    password = secrets["PWD"]
    security_token = secrets["UAT2_TOKEN" if uat == True else "PROD_TOKEN"]
    domain = 'test' if uat == True else None

    print("\nsf params:")
    print(f"username: {username}")
    print(f"password: {password}")
    print(f"security_token: {security_token}")
    print(f"domain: {domain}")

    return {"username": username, "password": password, "security token": security_token, "domain": domain}
