import json
import boto3
import base64
import simple_salesforce
import pandas as pd
from botocore.exceptions import ClientError


def handler(event, context):
    print("event:")
    print(event)
        
    property_id = event["propertyId"]
    USE_UAT = event["uat"]

    sf_secrets = json.loads(get_secret("sf_info"))
    username = sf_secrets["sf_user"] + (".uat2" if USE_UAT == True else "")
    password = sf_secrets["PWD"]
    security_token = sf_secrets["UAT2_TOKEN" if USE_UAT == True else "PROD_TOKEN"]
    domain = 'test' if USE_UAT == True else None

    print("sf params:")
    print(f"username: {username}")
    print(f"password: {password}")
    print(f"security_token: {security_token}")
    print(f"domain: {domain}")

    sf = simple_salesforce.Salesforce(username=username, password=password, security_token=security_token, domain=domain)

    doc_types_by_group = get_doc_types(sf)
    vendors = get_vendors(sf, property_id)

    print(f"doc types: {doc_types_by_group}")
    print(f"vendors: {vendors}")
    return {
        "statusCode": 200,
        "body": json.dumps({
            "docTypes": doc_types_by_group,
            "vendors": vendors
        })
    }
    

def get_secret(secret_name: str):
    region_name = "us-east-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        else:
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            secret = base64.b64decode(get_secret_value_response['SecretBinary'])
        return secret


def get_doc_types(sf):
    doc_types_query = "select Library__c, Document_Type__c from Library_Assignment__c"
    
    print(f"doc types query: {doc_types_query}")
    doc_type_results = sf.query_all(doc_types_query)["records"]
    print("doc types results:")
    print(doc_type_results)

    # Put doc_types into DataFrame for easy manipulation
    doc_types_df = pd.DataFrame(doc_type_results).drop(columns=["attributes"])

    # Organize document types by parent type
    groupby_library = doc_types_df.groupby(by="Library__c")
    doc_types_by_group = {}
    for doc_group, doc_types in groupby_library:
        data = list(doc_types["Document_Type__c"])
        data.sort()
        doc_types_by_group[doc_group] = data
    
    return doc_types_by_group


def get_vendors(sf, property_id):
    vendor_query = f"SELECT Id, Contact__c, Contact_Name__c, Contact_Type__c, Active_Vendor__c FROM Property_Contacts__c WHERE Property__r.name = '{property_id}'"
    print(f"\nvendor query: {vendor_query}")
    vendor_results = sf.query_all(vendor_query)["records"]
    print(f"vendor results: {vendor_results}")

    if len(vendor_results) == 0:
        print("Vendor query returned empty.")
        return []
    
    # Put vendors into DataFrame for easy manipulation
    vendors_df = pd.DataFrame(vendor_results).drop(columns=["attributes"])
    vendors = vendors_df.to_dict("records")
    
    return vendors
