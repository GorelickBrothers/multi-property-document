import json
import boto3
import base64
import urllib


s3 = boto3.client('s3')


def handler(event, context):
    print("event:")
    print(event)
    
    file_content, content_type = process_file(event)
    USE_UAT = event["uat"] == "True"

    num_slashes = len(event["name"].split("/")) - 1
    file_path = urllib.parse.quote_plus(event["name"].replace("/", "_", num_slashes - 1)).replace("+", " ") if num_slashes > 1 else event["name"]

    print(f"file content:")
    print(f"{file_content[:50]}{'...' if len(file_content) > 50 else ''}")
    print("file path:")
    print(file_path)

    # Print the bucket name and then put the file to s3
    BUCKET_NAME = f"homeworx-{'dev' if USE_UAT else 'prod'}"
    print(f"BUCKET_NAME: {BUCKET_NAME}")
    s3_response = s3.put_object(Bucket=BUCKET_NAME, Key=file_path, Body=file_content, ContentType=content_type)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "file_path": file_path,
            "s3_response": s3_response
        })
    }


def process_file(event: dict):
    """
    Converts the contents of the file to base64 and returns the
    file contents along with the file type based on the file extension
    """
    file_type = event["name"].split(".")[-1].lower()
    print(f"file type: {file_type}")
    
    if file_type in ["jpeg", "jpg", "png", "pdf"]:
        if file_type == "pdf":
            content_type = "application/pdf"
        elif file_type == "png":
            content_type = "image/png"
        elif file_type in ["jpeg", "jpg"]:
            content_type = "image/jpeg"
        
        print(f"Decoding file type: {file_type} to base64")
        file_content = base64.b64decode(event["document"])
        print(f"file content after decoding:")
        print(file_content)

        print(f"Split, convert to utf-8, then decode64:")
        file_content = file_content.split(b'base64,')[-1].decode("utf-8")
        file_content = base64.b64decode(file_content)
        print(f"file content after decoding:")
        print(file_content)

        content_type = f"application/{file_type}"
    else:
        file_content = event["document"]
        content_type = "application/json"
    
    return file_content, content_type
