export function getContentType(fileType) {
    switch (fileType) {
        case "pdf":
            return "application/pdf";
        case "png":
            return "image/png";
        case "jpg" || "jpeg":
            return "image/jpeg";
        default:
            return "application/json";
    }
}