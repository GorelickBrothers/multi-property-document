import { writable } from 'svelte/store';

export let mainStore = writable([]);
export let docTypesAndVendors = writable({});
export let USE_UAT = writable(false);
export let pageName = writable("");
export let objectType = writable("");

export let GET_PROPERTIES_URL = writable("");
export let GET_DOC_TYPES_AND_VENDORS_URL = writable("");
export let UPLOAD_DOC_URL = writable("");
export let CREATE_PROP_DOC_URL = writable("");

export let bulkPortfolio = writable({});

function createUploadedFiles() {
    const { subscribe, set } = writable([]);
    return {
        subscribe,
        set: (value) => set(Array.from(value))
    }
}

const uploadedFiles = createUploadedFiles();
export { uploadedFiles }